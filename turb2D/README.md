# Turbulence Simulation 2D

The simulations made are based on the simulations that are at `fluidsim` web page making some changes in the parameters.
## Stratified forcing turbulence

The simulation I did first is one using a stratified forcing. This simulation can be done by running the file `sim-start.py` where all the parameters for the simulation are. The file `graph.py` allows us to see some results of the simulation and those results can be seen in the folder `graph_strat`. 

In the results we can see the graphs showing the velocity field evolving throught time, at t=0, t=5, t=10, t=15, t=20, and t=25. Using this graph we can at t=0 we have some eddies ones going clockwise and other anti-clockwise, so thats the point of departure. Inmediatelly we see at t=5 that this vortices are all mixed up so we see smaller eddies this time, with the others times we see that this keeps evolving and the velocity keeps fluctuating.

There's also a graph showing the 1D spectra where most of the variables are show. The energy spectrum for x and z are really similar between them with a few differences. We see all the spectrum doesn't go to really small scales, it is still a normall cascade energy and it's fitted in a 5/3 power law.
