import os
import numpy as np
from fluidsim.solvers.ns2d.strat.solver import Simul
     
     
t_end = 25
nx = 64
 
params = Simul.create_default_params()

params.output.sub_directory = "examples"

ny = nx
Lx = 3
params.oper.nx = nx
params.oper.ny = ny
params.oper.Lx = Lx
params.oper.Ly = Ly = Lx / nx * ny

params.time_stepping.USE_T_END = True
params.time_stepping.t_end = t_end

n = 8
C = 1.0
dx = Lx / nx
B = 1
D = 1
eps = 1e-2 * B ** (3 / 2) * D ** (1 / 2)
params.nu_8 = (dx / C) ** ((3 * n - 2) / 3) * eps ** (1 / 3)

params.init_fields.type = "noise"
params.init_fields.noise.length = 1.0
params.init_fields.noise.velo_max = 0.1

params.forcing.enable = True
params.forcing.type = "tcrandom"
params.forcing.normalized.constant_rate_of = None
params.forcing.key_forced = "rot_fft"

params.output.periods_print.print_stdout = 1e-1

params.output.periods_save.phys_fields = 0.5
params.output.periods_save.spatial_means = 0.1
params.output.periods_save.spectra = 0.1
params.output.periods_save.spect_energy_budg = 0.1

sim = Simul(params)
sim.time_stepping.start()
#Review the code
