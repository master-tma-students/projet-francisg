import matplotlib.pyplot as plt

from pathlib import Path

from fluidsim import load

here = Path(__file__).absolute().parent
print(here)
fig_dir = here / "strat_graph"
fig_dir.mkdir(exist_ok=True)

path = "/home/francisco/Sim_data/examples/NS2D.strat_64x64_S3x3_2022-12-25_17-08-39"

sim = load(path)

## Velocity Field

sim.output.phys_fields.plot(time =0, numfig=0, nb_contours=25, cmap="turbo") #A cross-section of the z-component of vorticity
fig = plt.gcf()
fig.savefig(fig_dir / f"Velocity_Cross-seccion_t0.png")

sim.output.phys_fields.plot(time =5, numfig=0, nb_contours=25, cmap="turbo") #A cross-section of the z-component of vorticity
fig = plt.gcf()
fig.savefig(fig_dir / f"Velocity_Cross-seccion_t5.png")

sim.output.phys_fields.plot(time =10, numfig=0, nb_contours=25, cmap="turbo") #A cross-section of the z-component of vorticity
fig = plt.gcf()
fig.savefig(fig_dir / f"Velocity_Cross-seccion_t10.png")

sim.output.phys_fields.plot(time =15, numfig=0, nb_contours=25, cmap="turbo") #A cross-section of the z-component of vorticity
fig = plt.gcf()
fig.savefig(fig_dir / f"Velocity_Cross-seccion_t15.png")

sim.output.phys_fields.plot(time =20, numfig=0, nb_contours=25, cmap="turbo") #A cross-section of the z-component of vorticity
fig = plt.gcf()
fig.savefig(fig_dir / f"Velocity_Cross-seccion_t20.png")

sim.output.phys_fields.plot(time =25, numfig=0, nb_contours=25, cmap="turbo") #A cross-section of the z-component of vorticity
fig = plt.gcf()
fig.savefig(fig_dir / f"Velocity_Cross-seccion_t25.png")

## 1D Spectra

sim.output.spectra.plot1d(tmin=20, coef_compensate = 5/3)
fig = plt.gcf()
fig.savefig(fig_dir / f"1D_Spectra.png")


